# Código fuente de la página del Taller De Física al alcance de Todos

## Archivos binarios

Imagenes, documentos en format binario, y cualquier otro archivo que no sea texto plano (plain text),
es mejor subirlo a [la carpeta compartida](https://1drv.ms/u/s!AlIS1xuGgKAO9X2AuCzu_WAwhOEb?e=Ker1WQ) y no aquí.
