Title: TFAT usando Pelican en GitLab Pages!
Date: 2020-05-28
Category: GitLab
Tags: pelican, gitlab
Slug: tfat-en-gitlab-pages
Lang: es

Este sitio lo hosteamos en GitLab Pages!

El código del sitio lo encontrás en <https://gitlab.com/acdc1/tfat/website_pelican>.

Aprende más sobre GitLab Pages en <https://pages.gitlab.io>.
