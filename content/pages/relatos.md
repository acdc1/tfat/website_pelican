Title: Relatos
Date: 2020-06-05
Modified:  2020-07-07

**Experiencia de Dimitri Chafatinos**
  
<iframe width="560" height="315" src="https://www.youtube.com/embed/21wYYWUUSvM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  
  
**Laura Cruz contando su experiencia**
  
Laura Cruz (Primera mujer salteña egresada como Ingeniera Nuclear del Instituto Balseiro), Laury como todos la conocemos, contando lo que fue asistir al Taller de Fisica al alcance de Todos y su posterior historia hasta egresar del IB.
  
<iframe width="560" height="315" src="https://www.youtube.com/embed/U9Lx1JxdL20" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  
  
**Un físico perdido en la medicina // A physicist lost in medicine - por Alberto Villagrán**
  
Mini historia de mis comienzos en la fisica medica,  empezando en el taller de "Fisica al alcance de todos" del mejor profesor Daniel Cordoba en el año 2007 (Salta, Argentina).
  
Short story of my beginning in medical physics, starting from the course "Fisica al alcance de todos" of the best teacher Mr. Daniel Cordoba in the high school in 2007 (Salta, Argentina).
  
<iframe width="560" height="315" src="https://www.youtube.com/embed/UbyTojOl1sk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>