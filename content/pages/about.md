Title: Sobre el Taller
Date: 2020-05-28
Modified: 2020-05-28

![Anfiteatro lleno](https://4fmqia.bl.files.1drv.com/y4mYt_eEQH_IjsTO280o8CfN4dH0FgKv8px1cE_-PN-DeRmxeR8nOvZwq8oHadtD-MlGRCMyspsuiyDzpRU-tmSu7Wc5t7NelHoP_fy4jmT3kPNGakP4Dkx6Vvo4oMTac8XhZrTJTu9qE4Q3h5jgHoqRNAwnvRkwBaPN2K8orfGxXPz1stte0PnQQ4E9YSgKK2OG4f_tJ60_G4PpKdTwwkC9w?)

# Breve Historia

Al igual que ocurriera en muchas regiones de la Argentina, la implementación de la Ley Federal de Educación en Salta trajo aparejados problemas para la enseñanza de la Física y miles de jóvenes fueron impedidos de ser educados eficazmente en esa disciplina.

El número de horas de la asignatura disminuyó considerablemente, en otros casos nuestra disciplina perdió identidad al ser impartida en el área de Ciencias Naturales. En muchos colegios la misma desapareció de la currícula de formación de nuestros adolescentes, principalmente en instituciones  privadas que optaron por una modalidad distinta de las Ciencias Naturales. 

Esta situación particular, puso a nuestros jóvenes en una posición muy compleja en cuanto a su formación integral y su situación se veía comprometida en el caso que sus estudios superiores se orientaran hacia las carreras científico-tecnológicas.

En este contexto adverso para la enseñanza y aprendizaje de la Física, como una creación del Profesor Daniel Rubén Córdoba, en el año 1991 nace en el seno de  la Universidad Nacional de Salta El Taller “Física al Alcance de Todos” como un espacio diferenciado de Enseñanza de la Física. Destinado a jóvenes interesados en participar de las Olimpiadas de Física, donde el principal objetivo era la adquisición de conocimientos y destreza en la resolución de problemas no triviales para su edad. Este espacio educativo ya tenía en sus inicios parte de la esencia que hasta hoy lo acompaña: ***un ambiente no escolarizado, en donde la acreditación de saberes no es lo importante y se avanza de acuerdo a lo aprendido, respetando tiempos de aprendizaje.***

Se hace necesario destacar, que si bien la Olimpiada tiene aspectos competitivos, se pudo observar que los alumnos que comenzaron a acercarse al curso no lo hacían con ánimo de competir, sino que el ánimo que cruzaba sus intenciones era el de adquirir destrezas en la disciplina que le era vedada en sus colegios, y reconocían que era una actividad importante para el ingreso a la Universidad, de esta manera el carácter competitivo quedaba en un segundo plano, lo que importaba en sí, era acercar "el modo de ver" de nuestra disciplina a todos los jóvenes interesados, desafiándolos a trabajar en una actividad que les permitiría no solo el aprendizaje de conceptos y procedimientos propios de la disciplina sino que también los pondría a una edad temprana en el ambiente de la Universidad, además de permitir la promoción de vocaciones hacia la Ciencia y Tecnología para orientarlos y apoyarlos en sus estudios evitando que las mismas se pierdan.

Con los años el curso siguió su marcha y comenzaron a llegar estudiantes con expectativas de aprender algo de física. Comenzó a crecer la cantidad de estudiantes y ya no sólo fue necesario enseñar la resolución de problemas complejos a los estudiantes que pretendían participar en las Olimpiadas de Física, sino que había que contar qué era la física. Por ello, fue necesario organizar el curso en diferentes niveles. Esto implicó extender la jornada de trabajo a todo el sábado y se sumaron, además, otros días de la semana para consultas y laboratorio. Así fue que estudiantes que jamás se hubiesen acercado por un curso de Olimpiadas de Física se encontraron movilizados y enganchados con la disciplina. Se  consideró que ya no tenía sentido seguir convocando un Curso de Física para Olimpiadas y, así como se ampliaron las problemáticas abordadas y el número de estudiantes, el curso-taller pasó a llamarse “La Física al Alcance de Todos” donde “el único requisito era tener curiosidad y ganas de aprender. Y tal vez, la única exigencia, tener paciencia.”

El curso comenzó a cobrar notoriedad pública a partir de la participación destacada de sus estudiantes en las Olimpiadas Argentinas de Física y por el ingreso al Instituto Balseiro, de algunos estudiantes universitarios que fueron parte del taller. El mismo fue declarado de interés nacional por la Cámara de Senadores de la Nación y por la Cámara de Diputados de la Provincia de Salta. Recibió también, un reconocimiento de Alberto Sileoni, Ministro de Educación de la Nación, por promover vocaciones hacia la ciencia.

El profesor Daniel Córdoba fue declarado Dr. Honoris Causa por el Consejo Superior de la Universidad Nacional de Salta. También el Instituto Balseiro reconoció su trabajo como coordinador del curso por la Promoción de vocaciones por la Ciencia y la Tecnología.

Muchos de los estudiantes que pasaron por el taller hoy ocupan posiciones en organismos como el CONICET, la CNEA, diversas universidades nacionales de todo el país y también en entidades educativas y de investigación de Estados Unidos, Alemania, Canadá, España, Suiza, Inglaterra, Francia e Italia.

(Merece un blog post con todo el detalle)

## ¿Quién es Gofito?

![Gofito - Logo del taller](https://4lkpra.bl.files.1drv.com/y4mmflRNlva1JzlXnlGoaUB1yLNy4VCyLD-dbIScqDDkmPzyePwfEvoB-vXfvFj-VMvjr_ZZFlCVbLAHoR5woudgwcaNlNMb1_ElUK8ogSx9g_cIDsH6IpXLK9hns8Se9ARjjCeGjroWn9jI8fBiVqWjbYcPR7KBlIlX2EiNAEMgpkAOV4LzFn2VzwXa5KlQdyxGHJRXBnKk9tshNIRqKdE-nQuAvxVxbJZuepq9ZOP39A/TFAT-Logo.png)

# Estructura y funcionamiento

El taller está estructurado en cuatro (4) niveles:

**1er Nivel:** Curso de divulgación e inicios en el lenguaje del conocimiento científico. Dirigido a estudiantes de educación media que ingresan por primera vez al taller.

**2do Nivel:** Este curso tiene las mismas características que el 1er Nivel, pero está formado por estudiantes de educación media que llevan más de un año participando en el taller y que han tomado la decisión de no participar en el 3er Nivel.

En algunos años los primeros dos niveles compartían espacio y tiempo, impidiendo la superposición de temas tratados mediante un mecanismo de rotación entre dos grupos de temas. En otras etapas del taller los dos niveles se dictaron por separado.

**3er Nivel:** preparación de estudiantes de educación media para la participación en las Olimpíadas de Física.

**4to Nivel:** guía a estudiantes universitarios que pretenden prepararse para la obtención de becas en ciencia y tecnología.

# Equipo

Lista de la gente que participa y que quiere aparecer en este sitio.

