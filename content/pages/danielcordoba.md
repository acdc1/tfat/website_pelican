Title: ¿Quién fue Daniel Córdoba?
Date: 2020-06-05
Modified:  2020-06-05
Author: Maxi Baldiviezo

![FotoDaniel1](https://bl3302files.storage.live.com/y4mlDdSKTyAvH8BavFsbyGKf1vaukCaZMTPx9QdzBI-0CrQIMaNgUz6EEGg67RRKuv-aDVs5vBuZdNjPv_55M57e2JSwlVpUNmZj6yVlaR3o0RadeG4SFSAF5za5Ojf8qhPFbemdgYnuYqlQelODspO8Dhm2hLiXBQZl9TYuYxD2qH-UaRkvYT_25J9zNqKP0bUy-0fMfOwXjVoujcU3KzI9A/Daniel.jpg?psid=1&width=116&height=174)

![FotoDaniel2](https://bl3302files.storage.live.com/y4mrj_0XKVcY-RSDQh1ziEhFgAR3msffZZ97f7cc29lPkqt02_fs5_aVhDO2hCPOehPMHVdpY_qRDLiHR8eQ8f2qvXhPMgWrrp5EUfdF9PtPCNQSRsy4hZUmcPMM3-0TBgWAtqE0ngXqA-3beM-Jznh99kWTM6adATipdexBqqk5wtkmtvwPQ4tpIjrqFPgqrw7jcIL7nTNP1kpzKUCjDVv8g/ORFEOCordoba.jpg?psid=1&width=715&height=536)
