#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'TFAT team'
SITENAME = 'Taller de Física al Alcance de Todos'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

THEME = 'theme'

TIMEZONE = 'America/Argentina/Salta'

DEFAULT_LANG = 'es'

# Blog
INDEX_SAVE_AS = 'blog_index.html'

# Do not show categories on menu
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False
MENUITEMS = (
    ['Inicio', '/index.html'],
    ['El taller', '/pages/sobre-el-taller.html'],
    ['¿Quién fue Daniel Córdoba?', '/pages/quien-fue-daniel-cordoba.html'],
    ['Contacto', '/pages/contacto.html'],
  )

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ['Blog', '/blog_index.html'],
    ['Relatos', '/pages/relatos.html'],
    ['Archivo de clases', '/pages/archivo-de-clases.html'],
)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Plugins
PLUGIN_PATHS = ['plugins/', ]

PLUGINS = ['i18n_subsites', ]

JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}


